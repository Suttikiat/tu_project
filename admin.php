<style>
	/* TH Sarabun New */
	/* Regular */
	@font-face {
		font-family: 'th-sarabun-new';
		font-style: normal;
		font-weight: normal;
		src: url("/tu_project/fonts/TH_Sarabun_New/thsarabunnew.woff") format("woff");
	}

	/* Bold */
	@font-face {
		font-family: 'th-sarabun-new';
		font-weight: bold;
		font-style: normal;
		src: url("/tu_project/fonts/TH_Sarabun_New/thsarabunnew-bold.woff") format("woff");
	}

	/* Italic */
	@font-face {
		font-family: 'th-sarabun-new';
		font-weight: normal;
		font-style: italic;
		src: url("/tu_project/fonts/TH_Sarabun_New/thsarabunnew-italic.woff") format("woff");
	}

	/* Bold & Italic */
	@font-face {
		font-family: 'th-sarabun-new';
		font-weight: bold;
		font-style: italic;
		src: url("/tu_project/fonts/TH_Sarabun_New/thsarabunnew-bolditalic.woff") format("woff");
	}
	body, .btn{
		font-size: 20px;
		font-family: 'th-sarabun-new' !important;
	}
	.brand-text {
		font-size: 40px;
	}
	pre{
		font-family: 'th-sarabun-new' !important;
	}
	#convo{
		font-family: 'th-sarabun-new' !important;
		max-height: 80vh;
		overflow: auto;
	}
	#convo .card-title {
		font-size: 4rem;
	}
	#convo .card-header {
		background-color: rgba(0, 0, 0, 0.03);
	}
	#convo .card-body {
		margin-bottom: 15px;
	}
	#convo .card-footer {
		border-top: 1px solid rgba(0, 0, 0, 0.125);
		position: sticky;
		bottom: 0;
		background-color: #f8f9fa;
	}
	#convo .row {
		margin-left: 15px;
		margin-right: 15px;
	}
	#question_part, {
		margin-top: 15px;
		margin-bottom: 15px;
	}
	#convo .desc {
		font-size: 20px;
		margin-top: 15px;
		padding-bottom: 15px;
		border-bottom: 1px solid rgba(0, 0, 0, 0.125);
	}
	.each-question {
		margin-top: 15px;
		padding: 15px;
		border: 1px dotted #ccc;
		border-radius: 5px;
	}
	.m-t-15{
		margin-top: 15px;
	}
	.m-t-10{
		margin-top: 10px;
	}
	.m-b-15{
		margin-bottom: 15px;
	}
	.m-b-10{
		margin-bottom: 10px;
	}
	.p-l-0{
		padding-left: 0px;
	}
	.p-r-0{
		padding-right: 0px;
	}
	.p-t-0{
		padding-top: 0px;
	}
	.p-b-0{
		padding-bottom: 0px;
	}
	.red{
		color: red;
	}
	label.error {
		color: red;
		font-size: 18px;
		font-weight: normal;
	}

	.form-input:focus{
		outline: none;
	}
	.form-input{
		border: none !important;
		border-bottom: 1px dotted rgba(0, 0, 0, 0.5) !important;
	}
	.question-separate-line{
		padding-bottom: 15px;
		/*border-bottom: 1px solid rgba(0, 0, 0, 0.125);*/
	}
	.p-l-15{
		padding-left: 15px;
	}
	.q2-section {
		width: 100%;
		overflow: auto;
		border: 1px solid #ccc;
	}
	.no-lr-margin {
		margin-left: 0px !important;
		margin-right: 0px !important;
	}
	#myChart{
		width:100%;
		max-width:1000px;
		margin-left: auto;
		margin-right: auto;
	}
	#export_pdf_btn{
		float: right;
	}
	#back_btn{
		margin: auto;
	}

	button[type='submit'] {
		width: 70px;
		margin-right: 10px;
		border-top-right-radius: 0.25rem !important;
    	border-bottom-right-radius: 0.25rem !important;
	}

	.hide {
		display: none;
	}
	.center{
		text-align: center;
	}

	label {
		font-weight: normal !important;
	}

	pre {
		overflow-x: auto;
		white-space: pre-wrap;
		white-space: -moz-pre-wrap !important;
		white-space: -pre-wrap;
		white-space: -o-pre-wrap;
		word-wrap: break-word;
		padding: 0px;
		margin-bottom: 0px;
		font-size: 18px;
		border: none;
		background-color: unset;
		word-break: unset;
	}
	.swal2-container.swal2-top-end {
		max-width: 500px !important;
		display: inline-block;
		margin: 0px auto;
		position: fixed;
		transition: all 0.5s ease-in-out 0s;
		z-index: 3999;
		top: unset !important;
		bottom: 20px !important;
		left: 0px !important;
		right: 0px !important;
	}
	.swal2-top-end .swal2-icon {
		display: none !important;
	}
	.swal2-top-end .swal2-toast {
		max-width: unset;
	}
	.swal2-top-end .swal2-popup.swal2-toast{
		padding: 10px;
		font-size: 20pt;
	}
	.swal2-top-end .swal2-popup.swal2-toast .swal2-title {
		padding-left: 10px;
		font-size: 12pt;
	}
	.swal2-top-end .swal2-popup.swal2-toast .swal2-title{
		color: white;
	}
	.swal2-top-end .swal2-popup.swal2-toast.swal2-icon-warning {
	background-color: #fd7e14;
	}
	.swal2-top-end .swal2-popup.swal2-toast.swal2-icon-error {
	background-color: #dc3545;
	}
	.swal2-top-end .swal2-popup.swal2-toast.swal2-icon-success {
	background-color: #28a745;
	}
	.swal2-container.swal2-center button {
		width: 200px;
	}
	.swal2-center .swal2-popup {
		font-size: 1.125em;
	}
	.swal2-center .swal2-title {
		font-size: 1.875em;
	}
	.swal2-center .swal2-styled.swal2-confirm, .swal2-center .swal2-styled.swal2-cancel {
		font-size: 1.0625em;
		margin-bottom: 30px;
		padding: 10px;
	}
	.swal2-center .swal2-styled.swal2-cancel {
		color: #555;
		background-color: rgba(0,0,0,0.05) !important
	}
	.upload-file-form {
		position: absolute;
		z-index: 2;
		top: 0;
		left: 0;
		filter: alpha(opacity=0);
		-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		opacity: 0;
		background-color: transparent;
		color: transparent;
		min-height: 30px;
	}
	.upload-place { 
		position: relative;
	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card direct-chat direct-chat-primary" id="convo">
              <div class="card-header ui-sortable-handle" style="cursor: move;">
                <h2 class="card-title">Admin</h2>
              </div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12 desc">
							<h3><b>นำเข้าข้อมูล (Import Data)</b></h3>
							<span>
								นำเข้าข้อมูลคำตอบจากแบบสอบถามของ Google Form โดยเป็นไฟล์ Excel มาเพิ่มลงในฐานข้อมูล
							</span>
							<br><br>
							<a type="button" class="btn form-btn btn-warning waves-effect upload-place" >
								นำเข้าข้อมูล
								<form id="import_file_data" onsubmit="return false;">
									<input type="file" class="upload-file-form" id="imported_csv_file" name="imported_csv_file">
								</form>
							</a>
							<span class="upload-file-name" id="upload_file_info"></span>
						</div>
						<div class="col-lg-12 desc">
							<h3><b>ส่งออกข้อมูล (Export Data)</b></h3>
							<span>
								นำข้อมูลทั้งหมดในฐานข้อมูลมาบันทึกลงเป็นไฟล์ Excel .xls ที่มีรูปแบบเหมือน Excel ของ Google Form 
							</span>
							<br><br>
							<button id="export_csv_btn" type="button" class="btn form-btn btn-primary waves-effect" onclick="downloadAttachment('/classes/Master.php?f=export_excel')">ส่งออกข้อมูล</button>
						</div>
						<div class="col-lg-12 desc">
							<h3><b>สร้างกราฟ (Generate Graph)</b></h3>
							<span>
								นำข้อมูลทั้งหมดในฐานข้อมูลมาคำนวณและสร้างกราฟ จำเป็นต้องมีข้อมูลคำตอบในฐานข้อมูล
							</span>
							<br><br>
							<button id="graph_btn" type="button" class="btn form-btn btn-primary waves-effect" onclick="generateGraph();">สร้างกราฟข้อมูล</button>
						</div>
						<div class="col-lg-12 desc">
							<h3><b>ลบข้อมูล (Delete Data)</b></h3>
							<span>
								ลบข้อมูลคำตอบและการคำนวณทั้งหมดในฐานข้อมูลอย่างถาวร และไม่สามารถเรียกข้อมูลที่ถูกลบแล้วกลับคืนได้ โปรดระมัดระวัง
							</span>
							<br><br>
							<button id="delete_btn" type="button" class="btn form-btn btn-danger waves-effect" onclick="deleteAllData();">ลบข้อมูล</button>
						</div>
					</div>
				</div>
            </div>
			<canvas id="myChart" class="hide m-b-10"></canvas>
			<div class="m-t-10  m-b-10" style="display: flex;">
				<button id="back_btn" type="button" class="btn form-btn btn-primary waves-effect hide" onclick="location.reload();">กลับหน้าหลัก</button>
			</div>
		</div>
	</div>
</div>
<div style='width:1200px;'>
</div>
<script type="text/javascript">
	var previous_calculation;
	var question_code_order = [];

	$(document).ready(function(){
		$("#questionnaire_form").validate({
			focus: function () {
				$(this).closest('form').validate().settings.onkeyup = false;
			},
			blur: function () {
				$(this).closest('form').validate().settings.onkeyup = $.validator.defaults.onkeyup;
			},
			onfocusout: function (element) {
				this.element(element);
			},
			rules: {
			},
			messages: {
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			submitHandler: function(form) {
				$('.card-footer .btn-primary').prop('disabled', true);
				
				start_loader();
				$.ajax({
					url:_base_url_+'classes/Master.php?f=save_answer',
					data: {},
					method: 'POST',
					success:function(resp){
						resp = JSON.parse(resp);
						if(resp.result == 1){
							alert_toast("ส่งแบบสอบถามสำเร็จ",'success');
							form_data.total = resp.total;
							previous_calculation = resp.previous_data;
							calculateResult(form_data);
							//location.reload()
						}else{
							alert_toast("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",'error');
						}
						end_loader();
						$('.card-footer .btn-primary').prop('disabled', false);
					}
				})
				
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
				$('.card-footer .btn-primary').prop('disabled', false);
				end_loader();
			}
		});

		$("#import_file_data").validate({
			submitHandler: function(form) {
				var file_data = $("#imported_csv_file").prop("files")[0];
				var form_data = new FormData();
    			form_data.append("imported_csv_file", file_data);
				start_loader();
				$.ajax({
					url:_base_url_+'classes/Master.php?f=import_data',
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					success:function(resp){
						if(resp.result == 1){
							alert_toast("นำเข้าข้อมูลสำเร็จ",'success');
						}else{
							alert_toast(resp.message,'error');
						}
						$("#imported_csv_file").val('');
						end_loader();
					},
					error: function () {
						$("#imported_csv_file").val('');
						alert_toast("เกิดข้อผิดพลาด นำเข้าข้อมูลไม่สำเร็จ",'error');
						end_loader();
					}
				});  
			}
		});

		load_question();

		$("#imported_csv_file").change(function () {
			var file_data = $("#imported_csv_file").prop("files")[0];
			var filename = $("#imported_csv_file").prop('files')[0]['name'];
			$("#upload_file_info").html(filename);
			$('#import_file_data').submit();
		});

	});

	function load_question(){
		start_loader();
		$.ajax({
			url:_base_url_+"classes/Master.php?f=get_questions",
			method:"POST",
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			data:{},
			success:function(resp){
				if(resp.status == 1){
					resp.data.forEach((q, i) => {
						question_code_order.push(q.question_code);
					});
				}else{
					alert_toast("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",'error');
				}
				end_loader();
			},
			error:err=>{
				console.log(err)
				alert_toast("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",'error');
				end_loader();
			}
		})
	}

	function downloadAttachment(url) {
		window.open(_base_url_ + url, '_blank');
	}

	function generateGraph(){
		//must calculate from beginning
		start_loader();
		$('#graph_btn').prop('disabled', true);
		$.ajax({
			url:_base_url_+'classes/Master.php?f=get_all_data',
			data: {},
			method: 'POST',
			success:function(resp){
				resp = JSON.parse(resp);
				if(resp.result == 1 && resp.total > 0){
					previous_calculation = {
						sigma_YD_PD: 0,
						sigma_YA_PA: 0,
						sigma_YL_PL: 0,
						sigma_YD_PB: 0,
						sigma_YL_PS: 0,
						sigma_YA_PC: 0,
						sigma_YD_PD_square: 0,
						sigma_YA_PA_square: 0,
						sigma_YL_PL_square: 0,
						sigma_YD_PB_square: 0,
						sigma_YL_PS_square: 0,
						sigma_YA_PC_square: 0,
						total_participant: 1,
					}
					calculateResultFromDB(resp.data);
				}else{
					if(resp.message){
						alert_toast(resp.message,'error');
					}else {
						alert_toast("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",'error');
					}
				}
				end_loader();
				$('#graph_btn').prop('disabled', false);
			}
		})

		
	}

	function calculateResultFromDB(all_data){
		var graph_data = {};
		all_data.forEach((data, index) => {
			var form_data = {};
			var answer_str = data.answer_part2_1 +','+ data.answer_part2_2;
			var answer_list = answer_str.split(',');
			var score_YD = 0;
			var score_YA = 0;
			var score_YL = 0;
			var score_PD = 0;
			var score_PA = 0;
			var score_PL = 0;
			var score_PB = 0;
			var score_PS = 0;
			var score_PC = 0;

			var score_YD_PD = 0;
			var score_YA_PA = 0;
			var score_YL_PL = 0;
			var score_YD_PB = 0;
			var score_YL_PS = 0;
			var score_YA_PC = 0;

			//fix new formula 11/04/2024
			var scoreYL12 = 0;
			var scoreYL12_1 = 0;
			var scoreYA13 = 0;
			var scoreYA15 = 0;
			var scoreYA16 = 0;

			//calculate score for each type of answers by question code (x)
			question_code_order.forEach((code, i) => {
				var type = code.slice(0,2);
				if(type == 'YD'){
					score_YD = parseInt(score_YD + parseInt(answer_list[i]));
				}else if(type == 'YA'){
					score_YA = parseInt(score_YA + parseInt(answer_list[i]));
				}else if(type == 'YL'){
					score_YL = parseInt(score_YL + parseInt(answer_list[i]));
				}else if(type == 'PD'){
					score_PD = parseInt(score_PD + parseInt(answer_list[i]));
				}else if(type == 'PA'){
					score_PA = parseInt(score_PA + parseInt(answer_list[i]));
				}else if(type == 'PL'){
					score_PL = parseInt(score_PL + parseInt(answer_list[i]));
				}else if(type == 'PB'){
					score_PB = parseInt(score_PB + parseInt(answer_list[i]));
				}else if(type == 'PS'){
					score_PS = parseInt(score_PS + parseInt(answer_list[i]));
				}else if(type == 'PC'){
					score_PC = parseInt(score_PC + parseInt(answer_list[i]));
				}

				//fix new formula 11/04/2024
				if(code == 'YD5'){//YA13
					var spec_score = parseInt(answer_list[i]);
					spec_score = translateScoreNewCalculation(spec_score);
					scoreYA13 = spec_score;
				}else if(code == 'YD9'){//YA15
					var spec_score = parseInt(answer_list[i]);
					scoreYA15 = spec_score;
				}else if(code == 'PL2'){//YA16
					var spec_score = parseInt(answer_list[i]);
					spec_score = translateScoreNewCalculation(spec_score);
					scoreYA16 = spec_score;
				}else if(code == 'YL1'){//YD13
					var spec_score = parseInt(answer_list[i]);
					spec_score = translateScoreNewCalculation(spec_score);
					score_YD = parseInt(score_YD + spec_score);
				}else if(code == 'YL10'){//PB8
					var spec_score = parseInt(answer_list[i]);
					spec_score = translateScoreNewCalculation(spec_score);
					score_PB = parseInt(score_PB + spec_score);
				}else if(code == 'PD3'){//PS6
					var spec_score = parseInt(answer_list[i]);
					score_PD = parseInt(score_PD - spec_score);// remove PD3
					spec_score = translateScoreNewCalculation(spec_score);
					score_PS = parseInt(score_PS + spec_score);
				}else if(code == 'PS4'){//PC7
					var spec_score = parseInt(answer_list[i]);
					score_PS = parseInt(score_PS - spec_score);// remove PS4
					spec_score = translateScoreNewCalculation(spec_score);
					score_PC = parseInt(score_PC + spec_score);
				}else if(code == 'YL12'){//PB9, YL12_1
					var spec_score = parseInt(answer_list[i]);
					scoreYL12= spec_score;
					score_YL = parseInt(score_YL - spec_score);// remove YL12
					spec_score = translateScoreNewCalculation(spec_score);
					scoreYL12_1 = spec_score;
					score_PB = parseInt(score_PB + spec_score);// add PB9
				}
			});
			//โทสะ 
			var tosa = score_PA + score_YA + scoreYA13 + scoreYA15 + scoreYA16;
			//ราคะ
			var laka = score_PD + score_YD;
			//โมหะ
			var moha = score_PL + score_YL + scoreYL12_1;
			//วิตก
			var vitok = score_PB + score_YD;
			//พุทธะ
			var putta = score_PC + score_YA;
			//ศรัทธา
			var satta = score_PS + score_YL;

			//x
			score_YD_PD = laka / 16;
			score_YA_PA = tosa / 19;
			score_YL_PL = moha / 15;
			score_YD_PB = vitok / 21;
			score_YL_PS = satta / 17;
			score_YA_PC = putta / 19;
			//x square
			var YD_PD_square = score_YD_PD * score_YD_PD;
			var YA_PA_square = score_YA_PA * score_YA_PA;
			var YL_PL_square = score_YL_PL * score_YL_PL;
			var YD_PB_square = score_YD_PB * score_YD_PB;
			var YL_PS_square = score_YL_PS * score_YL_PS;
			var YA_PC_square = score_YA_PC * score_YA_PC;

			//sigma x then square
			var sigma_then_square_YD_PD = parseFloat(previous_calculation.sigma_YD_PD) + score_YD_PD;
			form_data.sigma_YD_PD = sigma_then_square_YD_PD;
			sigma_then_square_YD_PD = sigma_then_square_YD_PD * sigma_then_square_YD_PD;

			var sigma_then_square_YA_PA = parseFloat(previous_calculation.sigma_YA_PA) + score_YA_PA;
			form_data.sigma_YA_PA = sigma_then_square_YA_PA;
			sigma_then_square_YA_PA = sigma_then_square_YA_PA * sigma_then_square_YA_PA;

			var sigma_then_square_YL_PL = parseFloat(previous_calculation.sigma_YL_PL) + score_YL_PL;
			form_data.sigma_YL_PL = sigma_then_square_YL_PL;
			sigma_then_square_YL_PL = sigma_then_square_YL_PL * sigma_then_square_YL_PL;

			var sigma_then_square_YD_PB = parseFloat(previous_calculation.sigma_YD_PB) + score_YD_PB;
			form_data.sigma_YD_PB = sigma_then_square_YD_PB;
			sigma_then_square_YD_PB = sigma_then_square_YD_PB * sigma_then_square_YD_PB;

			var sigma_then_square_YL_PS = parseFloat(previous_calculation.sigma_YL_PS) + score_YL_PS;
			form_data.sigma_YL_PS = sigma_then_square_YL_PS;
			sigma_then_square_YL_PS = sigma_then_square_YL_PS * sigma_then_square_YL_PS;

			var sigma_then_square_YA_PC = parseFloat(previous_calculation.sigma_YA_PC) + score_YA_PC;
			form_data.sigma_YA_PC = sigma_then_square_YA_PC;
			sigma_then_square_YA_PC = sigma_then_square_YA_PC * sigma_then_square_YA_PC;

			//x square then sigma
			var square_then_sigma_YD_PD = parseFloat(previous_calculation.sigma_YD_PD_square) + YD_PD_square;
			var square_then_sigma_YA_PA = parseFloat(previous_calculation.sigma_YA_PA_square) + YA_PA_square;
			var square_then_sigma_YL_PL = parseFloat(previous_calculation.sigma_YL_PL_square) + YL_PL_square;
			var square_then_sigma_YD_PB = parseFloat(previous_calculation.sigma_YD_PB_square) + YD_PB_square;
			var square_then_sigma_YL_PS = parseFloat(previous_calculation.sigma_YL_PS_square) + YL_PS_square;
			var square_then_sigma_YA_PC = parseFloat(previous_calculation.sigma_YA_PC_square) + YA_PC_square;
			form_data.sigma_YD_PD_square = square_then_sigma_YD_PD;
			form_data.sigma_YA_PA_square = square_then_sigma_YA_PA;
			form_data.sigma_YL_PL_square = square_then_sigma_YL_PL;
			form_data.sigma_YD_PB_square = square_then_sigma_YD_PB;
			form_data.sigma_YL_PS_square = square_then_sigma_YL_PS;
			form_data.sigma_YA_PC_square = square_then_sigma_YA_PC;

			var total = parseInt(previous_calculation.total_participant);
			//เอาจำนวน ณ ตัวนี้ด้วยใส่ใน db temp
			form_data.total_participant = total + 1;
			var degree_freedom = total - 1;
			if(total == 1){
				degree_freedom = total;
			}
			
			//calculate SD
			var sd_YD_PD = Math.sqrt((square_then_sigma_YD_PD + (sigma_then_square_YD_PD / total)) / (degree_freedom));
			var sd_YA_PA = Math.sqrt((square_then_sigma_YA_PA + (sigma_then_square_YA_PA / total)) / (degree_freedom));
			var sd_YL_PL = Math.sqrt((square_then_sigma_YL_PL + (sigma_then_square_YL_PL / total)) / (degree_freedom));
			var sd_YD_PB = Math.sqrt((square_then_sigma_YD_PB + (sigma_then_square_YD_PB / total)) / (degree_freedom));
			var sd_YL_PS = Math.sqrt((square_then_sigma_YL_PS + (sigma_then_square_YL_PS / total)) / (degree_freedom));
			var sd_YA_PC = Math.sqrt((square_then_sigma_YA_PC + (sigma_then_square_YA_PC / total)) / (degree_freedom));

			//calculate x bar
			var x_bar_YD_PD = form_data.sigma_YD_PD / total;
			var x_bar_YA_PA = form_data.sigma_YA_PA / total;
			var x_bar_YL_PL = form_data.sigma_YL_PL / total;
			var x_bar_YD_PB = form_data.sigma_YD_PB / total;
			var x_bar_YL_PS = form_data.sigma_YL_PS / total;
			var x_bar_YA_PC = form_data.sigma_YA_PC / total;

			//calculate z-score
			var z_score_YD_PD = x_bar_YD_PD / sd_YD_PD;
			var z_score_YA_PA = x_bar_YA_PA / sd_YA_PA;
			var z_score_YL_PL = x_bar_YL_PL / sd_YL_PL;
			var z_score_YD_PB = x_bar_YD_PB / sd_YD_PB;
			var z_score_YL_PS = x_bar_YL_PS / sd_YL_PS;
			var z_score_YA_PC = x_bar_YA_PC / sd_YA_PC;

			//calculate t-score
			var t_score_YD_PD = (z_score_YD_PD * 10) + 50;
			var t_score_YA_PA = (z_score_YA_PA * 10) + 50;
			var t_score_YL_PL = (z_score_YL_PL * 10) + 50;
			var t_score_YD_PB = (z_score_YD_PB * 10) + 50;
			var t_score_YL_PS = (z_score_YL_PS * 10) + 50;
			var t_score_YA_PC = (z_score_YA_PC * 10) + 50;

			graph_data = {
				t_score_YD_PD: t_score_YD_PD,
				t_score_YA_PA: t_score_YA_PA,
				t_score_YL_PL: t_score_YL_PL,
				t_score_YD_PB: t_score_YD_PB,
				t_score_YL_PS: t_score_YL_PS,
				t_score_YA_PC: t_score_YA_PC
			}

			previous_calculation = form_data;
		});

		//gather data for graph don't update temp from admin
		createGraph(graph_data);
	}

	function translateScoreNewCalculation(spec_score){
		if(spec_score == 5){
			spec_score = 1;
		}else if(spec_score == 1){
			spec_score = 5;
		}else if(spec_score == 4){
			spec_score = 2;
		}else if(spec_score == 2){
			spec_score = 4;
		}
		return spec_score;
	}

	function createGraph(graph_data){
		var xValues = ["พุทธจริต", "วิตกจริต", "ศรัทธาจริต", "โมหะจริต", "โทสะจริต", "ราคะจริต"];
		var yValues = [graph_data.t_score_YD_PD, graph_data.t_score_YA_PA, graph_data.t_score_YL_PL, graph_data.t_score_YD_PB, graph_data.t_score_YL_PS, graph_data.t_score_YA_PC];
		new Chart("myChart", {
			type: "horizontalBar",
			data: {
				labels: xValues,
				datasets: [{
				backgroundColor: 'rgba(43, 44, 170, 1)',
				data: yValues
				}]
			},
			options: {
				legend: {display: false},
				title: {
					fontSize: 20,
					display: true,
					text: "T-score ของคุณลักษณะจริต 6"
				}
			}
		});
		$('#convo').empty();
		$('#back_btn').removeClass('hide');
		$('#myChart').removeClass('hide');
	}

	function deleteAllData(){
	var text = "คุณกำลังจะลบข้อมูลคำตอบทั้งหมดในฐานข้อมูล และไม่สามารถกู้คืนกลับมาได้" + "\n"+ "คุณต้องการลบข้อมูลใช่หรือไม่?";
	Swal.fire({
		title: "คุณจะลบข้อมูลหรือไม่?",
		text: text,
    	icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		confirmButtonText: "ยืนยันการลบข้อมูลทั้งหมด",
		closeOnConfirm: true,
		cancelButtonText: "ยกเลิก"
	}).then((result) => {
		if (result.isConfirmed){
			start_loader();
			$.ajax({
				url:_base_url_+'classes/Master.php?f=delete_all_data',
				data: {},
				method: 'POST',
				success:function(resp){
					resp = JSON.parse(resp);
					if(resp.result == 1){
						alert_toast("ลบข้อมูลสำเร็จ",'success');
					}else{
						alert_toast("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง",'error');
					}
					end_loader();
					$('#graph_btn').prop('disabled', false);
				}
			});  
		}
	});
}
	

</script>
