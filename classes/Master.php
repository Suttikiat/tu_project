<?php
include "PHPExcel.php";
include "PHPExcel/Worksheet.php";
require_once('../config.php');
Class Master extends DBConnection {
	private $settings;
	public function __construct(){
		global $_settings;
		$this->settings = $_settings;
		parent::__construct();
	}
	public function __destruct(){
		parent::__destruct();
	}

    private $answer_2_fields = array(
        'create_datetime' => 'ประทับเวลา',
        'q2_1' => '2.1 ลักษณะทั่วไปของจริต [การแต่งกายของท่านมีผลต่อความสำเร็จ]',
        'q2_2' => '2.1 ลักษณะทั่วไปของจริต [ท่านจะตัดบททันทีเมื่อคนพูดไม่ตรงประเด็น]',
        'q2_3' => '2.1 ลักษณะทั่วไปของจริต [ท่านจะไม่ยอมเปลี่ยนความคิดง่ายๆ]',
        'q2_4' => '2.1 ลักษณะทั่วไปของจริต [ท่านชอบกิจกรรมโลดโผน]',
        'q2_5' => '2.1 ลักษณะทั่วไปของจริต [ท่านชอบคิดวิเคราะห์แก้ปัญหา]',
        'q2_6' => '2.1 ลักษณะทั่วไปของจริต [ท่านชอบเดินเล่นไปเรื่อยๆ โดยไม่มีความทุกข์ร้อน]',
        'q2_7' => '2.1 ลักษณะทั่วไปของจริต [ท่านชอบทำงานตามกฎระเบียบ]',
        'q2_8' => '2.1 ลักษณะทั่วไปของจริต [ท่านเชื่อผู้อื่นได้ง่าย]',
        'q2_9' => '2.1 ลักษณะทั่วไปของจริต [ท่านดูแลเครื่องนอนให้เรียบร้อยทุกครั้ง]',
        'q2_10' => '2.1 ลักษณะทั่วไปของจริต [ท่านทำงานด้วยความปราณีต]',
        'q2_11' => '2.1 ลักษณะทั่วไปของจริต [ท่านทำงานได้ดีโดยเฉพาะงานประจำ]',
        'q2_12' => '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนขี้หงุดหงิดและโกรธง่าย]',
        'q2_13' => '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนเด็ดขาด]',
        'q2_14' => '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนพูดจริงทำจริง]',
        'q2_15' => '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนพูดจาโผงผาง]',
        'q2_16' => '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนพูดจาไพเราะอ่อนหวาน]',
        'q2_17' => '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนรับประทานอาหารเร็ว]',
        'q2_18' => '2.1 ลักษณะทั่วไปของจริต [ท่านมักทำอะไรด้วยความรวดเร็ว]',
        'q2_19' => '2.1 ลักษณะทั่วไปของจริต [ท่านมักทำอะไรตามกลุ่มเพื่อน]',
        'q2_20' => '2.1 ลักษณะทั่วไปของจริต [ท่านมักเร่งรีบทำงานให้เสร็จ]',
        'q2_21' => '2.1 ลักษณะทั่วไปของจริต [ท่านมีความสุขทุกครั้งเมื่อได้ชมสิ่งที่บันเทิงอารมณ์]',
        'q2_22' => '2.1 ลักษณะทั่วไปของจริต [ท่านมีอาการง่วงๆ ซึมๆ บ่อย]',
        'q2_23' => '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ค่อยมีสมาธิในการทำงาน]',
        'q2_24' => '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ชอบการเป็นผู้นำ]',
        'q2_25' => '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ชอบงานที่ต้องรับผิดชอบมาก]',
        'q2_26' => '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ชอบทำตัวเป็นจุดเด่น]',
        'q2_27' => '2.1 ลักษณะทั่วไปของจริต [ท่านเร่งรีบในการทำกิจวัตรเสมอ]',
        'q2_28' => '2.1 ลักษณะทั่วไปของจริต [ท่านสามารถชื่นชมสิ่งรอบตัวเสมอ]',
        'q2_29' => '2.1 ลักษณะทั่วไปของจริต [ท่านสามารถใช้คำพูดโต้แย้งได้อย่างเหมาะสม]' ,
        'q2_30' => '2.1 ลักษณะทั่วไปของจริต [ท่านสามารถประสานงานเรื่องต่าง ๆ ได้ดี]',
        'q2_31' => '2.1 ลักษณะทั่วไปของจริต [ท่านเหม่อลอยบ่อย]',
        'q2_32' => '2.1 ลักษณะทั่วไปของจริต [ท่านให้ความสำคัญของสีและกลิ่นของอาหาร]',
        'q2_33' => '2.1 ลักษณะทั่วไปของจริต [ท่านอยากทำสิ่งใดท่านจะลงมือทำทันที]',
        'q2_34' => '2.1 ลักษณะทั่วไปของจริต [บ่อยครั้งที่ท่านคิดวนไปวนมา]',
        'q2_35' => '2.1 ลักษณะทั่วไปของจริต [เวลาตื่นนอน ท่านจะเซื่องซึมงัวเงีย]',
        'q2_36' => '2.1 ลักษณะทั่วไปของจริต [เวลาท่านทานอาหาร ท่านจะมองไปรอบๆ]',

        'q2_37' => '2.2 ลักษณะเฉพาะของจริต [ท่านกังวลกับสิ่งที่คนอื่นไม่กังวล]',
        'q2_38' => '2.2 ลักษณะเฉพาะของจริต [ท่านใคร่ครวญทุกครั้งก่อนลงมือทำก่อน]',
        'q2_39' => '2.2 ลักษณะเฉพาะของจริต [ท่านชอบจินตนาการในเรื่องต่างๆ เสมอ]',
        'q2_40' => '2.2 ลักษณะเฉพาะของจริต [ท่านชอบตบแต่งสิ่งรอบตัวท่านให้สวยงาม]',
        'q2_41' => '2.2 ลักษณะเฉพาะของจริต [ท่านชอบเปลี่ยนที่ทำงานไปตามความคิดท่าน]',
        'q2_42' => '2.2 ลักษณะเฉพาะของจริต [ท่านชอบพูดเรื่องสัพเพเหระ]',
        'q2_43' => '2.2 ลักษณะเฉพาะของจริต [ท่านเชื่อมั่นในการเสียสละ]',
        'q2_44' => '2.2 ลักษณะเฉพาะของจริต [ท่านเชื่อมั่นในสิ่งที่จิตสัมผัสได้]',
        'q2_45' => '2.2 ลักษณะเฉพาะของจริต [ท่านทนไม่ได้เมื่อถูกเอาเปรียบ]',
        'q2_46' => '2.2 ลักษณะเฉพาะของจริต [ท่านทำงานตามความเป็นจริง]',
        'q2_47' => '2.2 ลักษณะเฉพาะของจริต [ท่านทำทุกสิ่งอย่างที่จะหลุดพ้นจากความทุกข์]',
        'q2_48' => '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนขี้รำคาญ]',
        'q2_49' => '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนใครดีดีด้วยใครร้ายร้ายตอบ]',
        'q2_50' => '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนมีเป้าหมายชีวิต]',
        'q2_51' => '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนละเอียดถี่ถ้วน]',
        'q2_52' => '2.2 ลักษณะเฉพาะของจริต [ท่านพิถีพิถันในความสวยความงาม]',
        'q2_53' => '2.2 ลักษณะเฉพาะของจริต [ท่านมักเก็บอารมณ์โกรธไม่ได้]',
        'q2_54' => '2.2 ลักษณะเฉพาะของจริต [ท่านมีความมุ่งมั่นและพร้อมสนับสนุนในเรื่องที่ดีงาม]',
        'q2_55' => '2.2 ลักษณะเฉพาะของจริต [ท่านมีความสุขมากเมื่อได้ฟังเรื่องราวของผู้ผ่านพ้นความทุกข์]',
        'q2_56' => '2.2 ลักษณะเฉพาะของจริต [ท่านไม่สร้างความเดือดร้อนให้กับตนเอง]',
        'q2_57' => '2.2 ลักษณะเฉพาะของจริต [ท่านไม่สามารถทนได้เมื่อถูกมองเป็นคนขี้เหร่]',
        'q2_58' => '2.2 ลักษณะเฉพาะของจริต [ท่านยอมรับฟังข้อแนะนำสั่งสอนที่เป็นประโยชน์ต่อตัวท่านเองเสมอ]',
        'q2_59' => '2.2 ลักษณะเฉพาะของจริต [ท่านยึดมั่นในความเชื่อของตน]',
        'q2_60' => '2.2 ลักษณะเฉพาะของจริต [ท่านรู้สึกเป็นสุขใจในการทำความดี]',
        'q2_61' => '2.2 ลักษณะเฉพาะของจริต [ท่านรู้สึกว่าตนเองมีความสามารถสูง]',
        'q2_62' => '2.2 ลักษณะเฉพาะของจริต [ท่านสร้างสมคุณงามความดีเสมอ]',
        'q2_63' => '2.2 ลักษณะเฉพาะของจริต [ท่านอึดอัดใจเมื่อต้องพูดถึงคุณความดีของตนเอง]',
        'q2_64' => '2.2 ลักษณะเฉพาะของจริต [ทำดีได้ดี ทำชั่วได้ชั่วไม่เป็นเรื่องจริงเสมอไป]'
    );

    private $translate_key = array(
        'ประทับเวลา' => 'create_datetime',
        'เพศ' => "gender",
        'อายุ' => "age",
        'สถานภาพ ' => "maritrial",
        'ระดับการศึกษา' => "education",
        'อาชีพ' => "job",
        '2.1 ลักษณะทั่วไปของจริต [การแต่งกายของท่านมีผลต่อความสำเร็จ]' => 'q2_1',
        '2.1 ลักษณะทั่วไปของจริต [ท่านจะตัดบททันทีเมื่อคนพูดไม่ตรงประเด็น]' => 'q2_2',
        '2.1 ลักษณะทั่วไปของจริต [ท่านจะไม่ยอมเปลี่ยนความคิดง่ายๆ]' => 'q2_3',
        '2.1 ลักษณะทั่วไปของจริต [ท่านชอบกิจกรรมโลดโผน]' => 'q2_4',
        '2.1 ลักษณะทั่วไปของจริต [ท่านชอบคิดวิเคราะห์แก้ปัญหา]' => 'q2_5',
        '2.1 ลักษณะทั่วไปของจริต [ท่านชอบเดินเล่นไปเรื่อยๆ โดยไม่มีความทุกข์ร้อน]' => 'q2_6',
        '2.1 ลักษณะทั่วไปของจริต [ท่านชอบทำงานตามกฎระเบียบ]' => 'q2_7',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเชื่อผู้อื่นได้ง่าย]' => 'q2_8',
        '2.1 ลักษณะทั่วไปของจริต [ท่านดูแลเครื่องนอนให้เรียบร้อยทุกครั้ง]' => 'q2_9',
        '2.1 ลักษณะทั่วไปของจริต [ท่านทำงานด้วยความปราณีต]' => 'q2_10',
        '2.1 ลักษณะทั่วไปของจริต [ท่านทำงานได้ดีโดยเฉพาะงานประจำ]' => 'q2_11',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนขี้หงุดหงิดและโกรธง่าย]' => 'q2_12',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนเด็ดขาด]' => 'q2_13',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนพูดจริงทำจริง]' => 'q2_14',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนพูดจาโผงผาง]' => 'q2_15',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนพูดจาไพเราะอ่อนหวาน]' => 'q2_16',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเป็นคนรับประทานอาหารเร็ว]' => 'q2_17',
        '2.1 ลักษณะทั่วไปของจริต [ท่านมักทำอะไรด้วยความรวดเร็ว]' => 'q2_18',
        '2.1 ลักษณะทั่วไปของจริต [ท่านมักทำอะไรตามกลุ่มเพื่อน]' => 'q2_19',
        '2.1 ลักษณะทั่วไปของจริต [ท่านมักเร่งรีบทำงานให้เสร็จ]' => 'q2_20',
        '2.1 ลักษณะทั่วไปของจริต [ท่านมีความสุขทุกครั้งเมื่อได้ชมสิ่งที่บันเทิงอารมณ์]' => 'q2_21',
        '2.1 ลักษณะทั่วไปของจริต [ท่านมีอาการง่วงๆ ซึมๆ บ่อย]' => 'q2_22',
        '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ค่อยมีสมาธิในการทำงาน]' => 'q2_23',
        '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ชอบการเป็นผู้นำ]' => 'q2_24',
        '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ชอบงานที่ต้องรับผิดชอบมาก]' => 'q2_25',
        '2.1 ลักษณะทั่วไปของจริต [ท่านไม่ชอบทำตัวเป็นจุดเด่น]' => 'q2_26',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเร่งรีบในการทำกิจวัตรเสมอ]' => 'q2_27',
        '2.1 ลักษณะทั่วไปของจริต [ท่านสามารถชื่นชมสิ่งรอบตัวเสมอ]' => 'q2_28',
        '2.1 ลักษณะทั่วไปของจริต [ท่านสามารถใช้คำพูดโต้แย้งได้อย่างเหมาะสม]' => 'q2_29',
        '2.1 ลักษณะทั่วไปของจริต [ท่านสามารถประสานงานเรื่องต่าง ๆ ได้ดี]' => 'q2_30',
        '2.1 ลักษณะทั่วไปของจริต [ท่านเหม่อลอยบ่อย]' => 'q2_31',
        '2.1 ลักษณะทั่วไปของจริต [ท่านให้ความสำคัญของสีและกลิ่นของอาหาร]' => 'q2_32',
        '2.1 ลักษณะทั่วไปของจริต [ท่านอยากทำสิ่งใดท่านจะลงมือทำทันที]' => 'q2_33',
        '2.1 ลักษณะทั่วไปของจริต [บ่อยครั้งที่ท่านคิดวนไปวนมา]' => 'q2_34',
        '2.1 ลักษณะทั่วไปของจริต [เวลาตื่นนอน ท่านจะเซื่องซึมงัวเงีย]' => 'q2_35',
        '2.1 ลักษณะทั่วไปของจริต [เวลาท่านทานอาหาร ท่านจะมองไปรอบๆ]' => 'q2_36',

        '2.2 ลักษณะเฉพาะของจริต [ท่านกังวลกับสิ่งที่คนอื่นไม่กังวล]' => 'q2_37',
        '2.2 ลักษณะเฉพาะของจริต [ท่านใคร่ครวญทุกครั้งก่อนลงมือทำก่อน]' => 'q2_38',
        '2.2 ลักษณะเฉพาะของจริต [ท่านชอบจินตนาการในเรื่องต่างๆ เสมอ]' => 'q2_39',
        '2.2 ลักษณะเฉพาะของจริต [ท่านชอบตบแต่งสิ่งรอบตัวท่านให้สวยงาม]' => 'q2_40',
        '2.2 ลักษณะเฉพาะของจริต [ท่านชอบเปลี่ยนที่ทำงานไปตามความคิดท่าน]' => 'q2_41',
        '2.2 ลักษณะเฉพาะของจริต [ท่านชอบพูดเรื่องสัพเพเหระ]' => 'q2_42',
        '2.2 ลักษณะเฉพาะของจริต [ท่านเชื่อมั่นในการเสียสละ]' => 'q2_43',
        '2.2 ลักษณะเฉพาะของจริต [ท่านเชื่อมั่นในสิ่งที่จิตสัมผัสได้]' => 'q2_44',
        '2.2 ลักษณะเฉพาะของจริต [ท่านทนไม่ได้เมื่อถูกเอาเปรียบ]' => 'q2_45',
        '2.2 ลักษณะเฉพาะของจริต [ท่านทำงานตามความเป็นจริง]' => 'q2_46',
        '2.2 ลักษณะเฉพาะของจริต [ท่านทำทุกสิ่งอย่างที่จะหลุดพ้นจากความทุกข์]' => 'q2_47',
        '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนขี้รำคาญ]' => 'q2_48',
        '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนใครดีดีด้วยใครร้ายร้ายตอบ]' => 'q2_49',
        '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนมีเป้าหมายชีวิต]' => 'q2_50',
        '2.2 ลักษณะเฉพาะของจริต [ท่านเป็นคนละเอียดถี่ถ้วน]' => 'q2_51',
        '2.2 ลักษณะเฉพาะของจริต [ท่านพิถีพิถันในความสวยความงาม]' => 'q2_52',
        '2.2 ลักษณะเฉพาะของจริต [ท่านมักเก็บอารมณ์โกรธไม่ได้]' => 'q2_53',
        '2.2 ลักษณะเฉพาะของจริต [ท่านมีความมุ่งมั่นและพร้อมสนับสนุนในเรื่องที่ดีงาม]' => 'q2_54',
        '2.2 ลักษณะเฉพาะของจริต [ท่านมีความสุขมากเมื่อได้ฟังเรื่องราวของผู้ผ่านพ้นความทุกข์]' => 'q2_55',
        '2.2 ลักษณะเฉพาะของจริต [ท่านไม่สร้างความเดือดร้อนให้กับตนเอง]' => 'q2_56',
        '2.2 ลักษณะเฉพาะของจริต [ท่านไม่สามารถทนได้เมื่อถูกมองเป็นคนขี้เหร่]' => 'q2_57',
        '2.2 ลักษณะเฉพาะของจริต [ท่านยอมรับฟังข้อแนะนำสั่งสอนที่เป็นประโยชน์ต่อตัวท่านเองเสมอ]' => 'q2_58',
        '2.2 ลักษณะเฉพาะของจริต [ท่านยึดมั่นในความเชื่อของตน]' => 'q2_59',
        '2.2 ลักษณะเฉพาะของจริต [ท่านรู้สึกเป็นสุขใจในการทำความดี]' => 'q2_60',
        '2.2 ลักษณะเฉพาะของจริต [ท่านรู้สึกว่าตนเองมีความสามารถสูง]' => 'q2_61',
        '2.2 ลักษณะเฉพาะของจริต [ท่านสร้างสมคุณงามความดีเสมอ]' => 'q2_62',
        '2.2 ลักษณะเฉพาะของจริต [ท่านอึดอัดใจเมื่อต้องพูดถึงคุณความดีของตนเอง]' => 'q2_63',
        '2.2 ลักษณะเฉพาะของจริต [ทำดีได้ดี ทำชั่วได้ชั่วไม่เป็นเรื่องจริงเสมอไป]' => 'q2_64'
    );

    private $new_key = array(
        'create_datetime',
        'q2_1',
        'q2_2',
        'q2_3',
        'q2_4',
        'q2_5',
        'q2_6',
        'q2_7',
        'q2_8',
        'q2_9',
        'q2_10',
        'q2_11',
        'q2_12',
        'q2_13',
        'q2_14',
        'q2_15',
        'q2_16',
        'q2_17',
        'q2_18',
        'q2_19',
        'q2_20',
        'q2_21',
        'q2_22',
        'q2_23',
        'q2_24',
        'q2_25',
        'q2_26',
        'q2_27',
        'q2_28',
        'q2_29',
        'q2_30',
        'q2_31',
        'q2_32',
        'q2_33',
        'q2_34',
        'q2_35',
        'q2_36',
        'q2_37',
        'q2_38',
        'q2_39',
        'q2_40',
        'q2_41',
        'q2_42',
        'q2_43',
        'q2_44',
        'q2_45',
        'q2_46',
        'q2_47',
        'q2_48',
        'q2_49',
        'q2_50',
        'q2_51',
        'q2_52',
        'q2_53',
        'q2_54',
        'q2_55',
        'q2_56',
        'q2_57',
        'q2_58',
        'q2_59',
        'q2_60',
        'q2_61',
        'q2_62',
        'q2_63',
        'q2_64'
    );

    private $score_array = array(
        "มากที่สุด" => 5,
        "มาก" => 4,
        "ปานกลาง" => 3,
        "น้อย" => 2,
        "น้อยที่สุด" => 1
    );

	public function getAllData(){
		try {
            $qry = $this->conn->query("SELECT id, answer_part2_1, answer_part2_2, create_datetime FROM `answer_sheet`");
            $data = $qry->fetch_all(MYSQLI_ASSOC);
            if(!empty($data)){
                $resp["result"] = 1;
                $resp["data"] = $data;
                $resp["total"] = count($data);
            }else {
                $resp['result'] = 2;
                $resp['message'] = 'ไม่พบข้อมูลคำตอบในฐานข้อมูล กรุณาตอบแบบสอบถาม หรือนำเข้าข้อมูลก่อน';
            }
		}catch(PDOException $e){
			$resp["result"] = 2;
		}
		return json_encode($resp);
	}

	public function get_questions(){
		extract($_POST);
		$resp = array();
		$qry = $this->conn->query("SELECT id, question, question_code, type FROM `questions` where status = 'Y'");
		if($qry){
			$resp['status'] = 1;
			$resp['data'] = $qry->fetch_all(MYSQLI_ASSOC);
		}else{
			$resp['status'] = 2;
			$this->conn->error;
		}
		return json_encode($resp);
	}

	public function save_answer(){
		extract($_POST);
		$req = $_POST;
		
		$data = "";
        $date_time = date_create();
        $date_time = date_format($date_time,"Y-m-j H:i:s");

		$ins_resp = $this->conn->query("INSERT INTO `answer_sheet` set 
			answer_part2_1 = '{$part2["answer1"]}', 
			answer_part2_2 = '{$part2["answer2"]}', 
			create_datetime = '{$date_time}'
		");
		
		if(isset($ins_resp)){
			$this->settings->set_flashdata("success","ส่งแบบสอบถามสำเร็จ");
			$qry = $this->conn->query("SELECT COUNT(id) as count FROM `answer_sheet`");
			$total = $qry->fetch_all(MYSQLI_ASSOC)[0]['count'];

			$prev_calc = $this->getPreviousCalculation($_POST);
			$resp = array(
				"result" => 1,
				"total" => $total,
				"previous_data" => $prev_calc
			);
			return json_encode($resp);
		}else{
			$resp = array(
				"result" => 2
			);
			return json_encode($resp);
			exit;
		}
	}

	public function getPreviousCalculation($req = array()){
		extract($req);
		$qry = $this->conn->query("SELECT COUNT(id) as count FROM `temp_calculation`");
		$count = $qry->fetch_all(MYSQLI_ASSOC)[0]['count'];
		$qry = null;
        $date_time = date_create();
        $date_time = date_format($date_time,"Y-m-j H:i:s");

		//load previous calculation
		if($count){
			$qry = $this->conn->query("SELECT * FROM `temp_calculation`");
			$data = $qry->fetch_array();
			return $data;
		}else{//if no previous, save new
			$qry = $this->conn->query("INSERT INTO `temp_calculation` SET
				sigma_YD_PD = 0,
				sigma_YA_PA = 0,
				sigma_YL_PL = 0,
				sigma_YD_PB = 0,
				sigma_YL_PS = 0,
				sigma_YA_PC = 0,
				sigma_YD_PD_square = 0,
				sigma_YA_PA_square = 0,
				sigma_YL_PL_square = 0,
				sigma_YD_PB_square = 0,
				sigma_YL_PS_square = 0,
				sigma_YA_PC_square = 0,
				total_participant = 0,
				update_datetime = '{$date_time}'
			");
			if(isset($qry)){
				$qry = null;
				$qry = $this->conn->query("SELECT * FROM `temp_calculation`");
				$data = $qry->fetch_array();
				return $data;
			}else {
				return array();
			}
		}
	}

	public function update_temp(){
		extract($_POST);
		$qry = null;
		$qry = $this->conn->query("SELECT id FROM `temp_calculation`");
		$id = $qry->fetch_array()["id"];
        $date_time = date_create();
        $date_time = date_format($date_time,"Y-m-j H:i:s");
		
		//update
		$update_qry = $this->conn->query("UPDATE `temp_calculation` SET 
		sigma_YD_PD = {$sigma_YD_PD},
		sigma_YA_PA = {$sigma_YA_PA},
		sigma_YL_PL = {$sigma_YL_PL},
		sigma_YD_PB = {$sigma_YD_PB},
		sigma_YL_PS = {$sigma_YL_PS},
		sigma_YA_PC = {$sigma_YA_PC},
		sigma_YD_PD_square = {$sigma_YD_PD_square},
		sigma_YA_PA_square = {$sigma_YA_PA_square},
		sigma_YL_PL_square = {$sigma_YL_PL_square},
		sigma_YD_PB_square = {$sigma_YD_PB_square},
		sigma_YL_PS_square = {$sigma_YL_PS_square},
		sigma_YA_PC_square = {$sigma_YA_PC_square},
		total_participant = {$total_participant},
		update_datetime = '{$date_time}'
		where id = {$id}");
		
		if(isset($update_qry)){
			$resp = array(
				"result" => 1
			);
		}else {
			$resp = array(
				"result" => 2
			);
		}
		return json_encode($resp);
		exit;
	}

	public function exportDataToExcel(){
        $all_data = json_decode($this->getAllData(),true);
        if($all_data["result"] != 1){
            return "ไม่พบข้อมูลคำตอบในฐานข้อมูล กรุณาตอบแบบสอบถาม หรือนำเข้าข้อมูลก่อน";
        }
        $all_data = $all_data["data"];
        $all_data = $this->translateDataGoogleExcel($all_data);
        $phpexcel = new \PHPExcel();
        //SET header Style
		$phpexcel->setActiveSheetIndex(0);
        $phpexcel->getActiveSheet()->setTitle('การตอบแบบฟอร์ม1');
        $row = 1;

        $count_column = count($this->answer_2_fields);
        $max_column = \PHPExcel_Cell::stringFromColumnIndex($count_column-1); 

        $phpexcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
        $phpexcel->getActiveSheet()->getStyle("A".$row)->getFont()->setBold(true)->setName('Arial')->setSize(15);

		$phpexcel->getActiveSheet()->getStyle("A".$row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $col = 0;

        $answer_2_fields = $this->answer_2_fields;
        $phpexcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $phpexcel->getActiveSheet()->getRowDimension($row+1)->setRowHeight(20);

        $background_color = "cfe2f3";

        foreach($answer_2_fields as $key => $value){
            $column = \PHPExcel_Cell::stringFromColumnIndex($col); 
			$key = strtolower($key);
            $width = 20;
            $phpexcel->getActiveSheet()->setCellValue( $column.$row, $value ); 
            $phpexcel->getActiveSheet()->getColumnDimension($column)->setWidth($width);
			$phpexcel->getActiveSheet()->getStyle($column.$row)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
			$phpexcel->getActiveSheet()->getStyle($column.$row)->getFill()->getStartColor()->setRGB($background_color);
            $phpexcel->getActiveSheet()->getStyle($column.$row)->getFont()->setBold(true)->setName('Arial')->setSize(10);
            $col++;
        }

        $row = $row + 1;
        $data_row = count($all_data);
        $data_count = 0;
        $max_data_row = $row + $data_row;

        for($i = $row; $i < $max_data_row; $i++){
            $col = 0;
            $number_of_data = $data_count + 1;
            foreach($all_data[$number_of_data-1] as $data){
                $column = \PHPExcel_Cell::stringFromColumnIndex($col);
                $phpexcel->getActiveSheet()->setCellValue( $column.$i, $data);
                $col++;
            }
            $phpexcel->getActiveSheet()->getRowDimension($i)->setRowHeight(25);
            $data_count++;
        }
        $phpexcel->getActiveSheet()->getStyle("A".(1).":".$column.($max_data_row))->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
        $phpexcel->getActiveSheet()->getStyle("A".(1).":".$column.($max_data_row))->getAlignment()->applyFromArray(
			array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER )
        );

        header('Content-Type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="แบบสำรวจคุณลักษณะจริต6.xls"');
		header('Set-Cookie: fileDownload=true; path=/'); 
		header('Cache-Control: max-age=90, must-revalidate');
	 
		$objWriter = \PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
        $objWriter->save('php://output');
    }

	

    function translateDataGoogleExcel($all_data){
        $temp = array();
        $tmp = array();
        foreach($all_data as $key => $data){
            $date_time = date_create($data["create_datetime"]);
            $tmp["create_datetime"] = date_format($date_time,"j/n/Y H:i:s");

            $time = explode(' ', $tmp["create_datetime"]);
            $hour = date_format($date_time,"H");
            $str = str_split($hour);
            if($str[0] == '0'){
                $hour = $str[1];
            }
            $min = date_format($date_time,"i");
            $str = str_split($min);
            if($str[0] == '0'){
                $min = $str[1];
            }
            $sec = date_format($date_time,"s");
            $str = str_split($sec);
            if($str[0] == '0'){
                $sec = $str[1];
            }
            $time[1] = $hour . ':' . $min . ':' . $sec;
            $tmp["create_datetime"] = $time[0] . ' ' . $time[1];

            $answer_str = $data["answer_part2_1"] .','. $data["answer_part2_2"];
		    $answer_list = explode(',', $answer_str);
            foreach($answer_list as $i => $answer){
                if($answer == 5){
                    $answer_list[$i] = "มากที่สุด";
                }else if($answer == 4){
                    $answer_list[$i] = "มาก";
                }else if($answer == 3){
                    $answer_list[$i] = "ปานกลาง";
                }else if($answer == 2){
                    $answer_list[$i] = "น้อย";
                }else if($answer == 1){
                    $answer_list[$i] = "น้อยที่สุด";
                }
                $tmp[$i+1] = $answer_list[$i];
            }

            array_push($temp, $tmp);
        }
        return $temp;
    }

    function uploadExcel(){
        $target_dir = dirname(__DIR__) . "/uploads/import_excel/";
        if(!empty($_FILES)){
            $target_file = $target_dir . basename($_FILES["imported_csv_file"]["name"]);
            $uploadOk = 1;
        }else {
            $resp['result'] = 2;
            $resp['message'] = "ไม่มีไฟล์ที่ถูกเลือก กรุณาอัพโหลดไฟล์";
            return json_encode($resp);
        }

        if(!empty($_FILES["imported_csv_file"])){
            $extension = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            $allowed_extension = array(
                "xls",
                "xlsx"
            );
            // Allow certain file formats
            if(in_array($extension, $allowed_extension)){
                if ($_FILES["imported_csv_file"]["size"] <= 50000000){
                    if (move_uploaded_file($_FILES["imported_csv_file"]["tmp_name"], $target_file)) {
                        $file_type = \PHPExcel_IOFactory::identify($target_file);
                        $objReader = \PHPExcel_IOFactory::createReader($file_type);
                        $objReader->setReadDataOnly(true);
                        $objPHPExcel = $objReader->load($target_file);
                        $objPHPExcel->setActiveSheetIndex(0);
                        $highest_column = $objPHPExcel->getActiveSheet()->getHighestColumn();
                        $highest_row = $objPHPExcel->getActiveSheet()->getHighestRow();
                        
                        $header_excel = $objPHPExcel->getActiveSheet()->rangeToArray('A1:'.$highest_column.'1',null, true, true, true);
                        $header_excel = $header_excel[1];
                        $header_excel = array_filter($header_excel, function($ele) {
                            return $ele != null;
                        });
                        
                        foreach($header_excel as $key => $header) {//translate
                            if($header){
                                $str = str_replace($header, $this->translate_key[$header], $header);
                            }
                            $header_excel[$key] = $str;
                        }

                        //validate header
                        foreach($this->new_key as $key){
                            if(!in_array($key, $header_excel)){
                                unlink($target_file);
                                $resp['result'] = 2;
                                $resp['message'] = "รูปแบบ header ของข้อมูลในไฟล์ excel ไม่ถูกต้อง";
                                return json_encode($resp);
                            }
                        }

                        $r = -1;
                        $excel_data = array();
                        for ($row = 2; $row <= $highest_row; ++$row) {
                            $dataRow = $objPHPExcel->getActiveSheet()->rangeToArray('A'.$row.':'.$highest_column.$row,null, true, true, true);
                            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                                ++$r;
                                foreach($header_excel as $key => $header) {
                                    $excel_data[$r][$header] = $dataRow[$row][$key];
                                }
                            }
                        }
                        
                        $params = array();
                        foreach($excel_data as $index => $data) {
                            $answer_part2_1 = '';
                            $answer_part2_2 = '';
                            $key_list = array_keys($data);
                            foreach($key_list as $key){
                                if(!in_array($key, $this->new_key)){//remove unused
                                    unset($excel_data[$index][$key]);
                                }
                                $pos = strpos($key, "q2_"); 
                                if($pos === 0 && $data[$key]){
                                    //translate word to score
                                    $tran_score = $this->translateTextToScore($data[$key]);
                                    //validate data
                                    if($tran_score == ''){
                                        unlink($target_file);
                                        $resp['result'] = 2;
                                        $resp['message'] = "รูปแบบ data ของข้อมูลในไฟล์ excel ไม่ถูกต้อง";
                                        return json_encode($resp);
                                    }
                                    $excel_data[$index][$key] = $tran_score;
                                    $num = explode("q2_", $key);
                                    if(intval($num[1]) < 37){//ต้องใช้วิธีนับ answer_2_fields รับจำนวนที่นำหน้า 2.1, 2.2
                                        $answer_part2_1 = $answer_part2_1 . $tran_score . ',';
                                    }else {
                                        $answer_part2_2 = $answer_part2_2 . $tran_score . ',';
                                    }
                                }
                            }
                            $datetime = PHPExcel_Style_NumberFormat::toFormattedString($data["create_datetime"], 'YYYY-MM-DD H:i:s');
                            $datetime = date("Y-m-d H:i:s", strtotime(str_replace('/','-',$datetime)));
                            $excel_data[$key]["create_datetime"] = $datetime;

                            $params[] = array(
                                "create_datetime" => $datetime,
                                "answer_part2_1" => substr($answer_part2_1, 0, -1),
                                "answer_part2_2" => substr($answer_part2_2, 0, -1)
                            );
                        }
                        //insert data
                        foreach($params as $data){
                            $this->conn->begin_transaction();
                            $ins_resp = $this->insertImportData($data);
                            $prev_calc = $this->getPreviousCalculation();

                            if($ins_resp == 1 && !empty($prev_calc)){
                                //re-calulate 
                                $cal_result = $this->calculateResult($data, $prev_calc);
                                if($cal_result["result"] == 1){
                                    //update temp
                                    $qry = null;
                                    $qry = $this->conn->query("SELECT id FROM `temp_calculation`");
                                    $id = $qry->fetch_array()["id"];
                                    $date_time = date_create();
                                    $date_time = date_format($date_time,"Y-m-j H:i:s");
                                    
                                    //update
                                    $data_temp = $cal_result["data"];
                                    $update_qry = $this->conn->query("UPDATE `temp_calculation` SET 
                                    sigma_YD_PD = {$data_temp["sigma_YD_PD"]},
                                    sigma_YA_PA = {$data_temp["sigma_YA_PA"]},
                                    sigma_YL_PL = {$data_temp["sigma_YL_PL"]},
                                    sigma_YD_PB = {$data_temp["sigma_YD_PB"]},
                                    sigma_YL_PS = {$data_temp["sigma_YL_PS"]},
                                    sigma_YA_PC = {$data_temp["sigma_YA_PC"]},
                                    sigma_YD_PD_square = {$data_temp["sigma_YD_PD_square"]},
                                    sigma_YA_PA_square = {$data_temp["sigma_YA_PA_square"]},
                                    sigma_YL_PL_square = {$data_temp["sigma_YL_PL_square"]},
                                    sigma_YD_PB_square = {$data_temp["sigma_YD_PB_square"]},
                                    sigma_YL_PS_square = {$data_temp["sigma_YL_PS_square"]},
                                    sigma_YA_PC_square = {$data_temp["sigma_YA_PC_square"]},
                                    total_participant = {$data_temp["total_participant"]},
                                    update_datetime = '{$date_time}'
                                    where id = {$id}");
                                    
                                    if(isset($update_qry)){
                                        $resp['result'] = 1;
                                        $resp['message'] = "อัพโหลดไฟล์สำเร็จ";
                                    }else {
                                        $resp['result'] = 2;
                                        $resp['message'] = "ไม่สามารถนำเข้าข้อมูลใหม่ได้";
                                    }
                                }else {
                                    $resp['result'] = 2;
                                    $resp['message'] = "ไม่สามารถนำเข้าข้อมูลใหม่ได้";
                                }
                            }else {
                                $resp['result'] = 2;
                                $resp['message'] = "ไม่สามารถนำเข้าข้อมูลใหม่ได้";
                            }
                        }
                        //last
                        if($resp['result'] == 1){
                            unlink($target_file);
                            $this->conn->commit();
                        }else {
                            unlink($target_file);
                            $this->conn->rollback();
                        }
                    } else {
                        $resp['result'] = 2;
                        $resp['message'] = "อัพโหลดไฟล์ไม่สำเร็จ ไม่สามารถสร้างไฟล์ที่ tu_project/uploads/import_excel/ เนื่องจากระบบไม่อนุญาต";
                    }
                }
                else {
                    $resp['result'] = 2;
                    $resp['message'] = "ไฟล์มีขนาดใหญ่เกินไป อนุญาตไฟล์ขนาดไม่เกิน 50MB";
                }
            }
            else {
                $resp['result'] = 2;
                $resp['message'] = "อนุญาตเฉพาะไฟล์นามสกุล .xls หรือ .xlsx";
            }
        }
        else {
            $resp['result'] = 2;
            $resp['message'] = "ไม่มีไฟล์ที่ถูกเลือก กรุณาอัพโหลดไฟล์";
        }
        return json_encode($resp);
	}

    public function translateTextToScore($text)
    {
        foreach($this->score_array as $t => $score){
            if ($text == $t){
                return $score;
            }
        }
        
        return '';
    }

    public function insertImportData($params){
        $ins_resp = $this->conn->query("INSERT INTO `answer_sheet` set 
            answer_part2_1 = '{$params["answer_part2_1"]}', 
            answer_part2_2 = '{$params["answer_part2_2"]}', 
            create_datetime = '{$params["create_datetime"]}'
        ");
        
        if(isset($ins_resp)){
            return 1;
        }else {
            return 2;
        }
    }

    function calculateResult($data, $previous_calculation){
        //get question code list
        $qry_question = $this->conn->query("SELECT id, question, question_code, type FROM `questions` where status = 'Y'");
		if($qry_question){
			$question_list = $qry_question->fetch_all(MYSQLI_ASSOC);
		}else {
            $cal_result["result"] = 2;
        }
        $question_code_order = array();
        foreach($question_list as $index => $q){
            array_push($question_code_order, $q["question_code"]);
        }

		$form_data = array();
		$answer_str = $data["answer_part2_1"] .','. $data["answer_part2_2"];
		$answer_list = explode(',',$answer_str);
		$score_YD = 0;
		$score_YA = 0;
		$score_YL = 0;
		$score_PD = 0;
		$score_PA = 0;
		$score_PL = 0;
		$score_PB = 0;
		$score_PS = 0;
		$score_PC = 0;

		$score_YD_PD = 0;
		$score_YA_PA = 0;
		$score_YL_PL = 0;
		$score_YD_PB = 0;
		$score_YL_PS = 0;
		$score_YA_PC = 0;

        //fix new formula 11/04/2024
        $scoreYL12 = 0;
        $scoreYL12_1 = 0;
        $scoreYA13 = 0;
        $scoreYA15 = 0;
        $scoreYA16 = 0;

		//calculate score for each type of answers by question code (x)
		foreach($question_code_order as $i => $code) {
			$type = substr($code, 0, 2);
			if($type == 'YD'){
				$score_YD = intval($score_YD + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'YA'){
				$score_YA = intval($score_YA + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'YL'){
				$score_YL = intval($score_YL + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'PD'){
				$score_PD = intval($score_PD + intval($answer_list[$i]));
                error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'PA'){
				$score_PA = intval($score_PA + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'PL'){
				$score_PL = intval($score_PL + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'PB'){
				$score_PB = intval($score_PB + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'PS'){
				$score_PS = intval($score_PS + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}else if($type == 'PC'){
				$score_PC = intval($score_PC + intval($answer_list[$i]));
                //error_log(print_r($code." = ".$answer_list[$i],true));
			}

			if($code == 'YD5'){//YA13
				$spec_score = intval($answer_list[$i]);
				$spec_score = $this->translateScoreNewCalculation($spec_score);
                $scoreYA13 = $spec_score;
                //error_log(print_r("YA13 TO tosa = ".$scoreYA13,true));
			}else if($code == 'YD9'){//YA15
				$spec_score = intval($answer_list[$i]);
                $scoreYA15 = $spec_score;
                //error_log(print_r("YA15 TO tosa = ".$scoreYA15,true));
			}else if($code == 'PL2'){//YA16
				$spec_score = intval($answer_list[$i]);
				$spec_score = $this->translateScoreNewCalculation($spec_score);
                $scoreYA16 = $spec_score;
                //error_log(print_r("YA16 TO tosa = ".$scoreYA16,true));
			}else if($code == 'YL1'){//YD13
				$spec_score = intval($answer_list[$i]);
				$spec_score = $this->translateScoreNewCalculation($spec_score);
				$score_YD = intval($score_YD + $spec_score);
                //error_log(print_r("YD13 = ".$spec_score,true));
			}else if($code == 'YL10'){//PB8
				$spec_score = intval($answer_list[$i]);
				$spec_score = $this->translateScoreNewCalculation($spec_score);
				$score_PB = intval($score_PB + $spec_score);
                //error_log(print_r("PB8 = ".$spec_score,true));
			}else if($code == 'PD3'){//PS6
				$spec_score = intval($answer_list[$i]);
				$score_PD = intval($score_PD - $spec_score);// remove PD3
                //error_log(print_r("REMOVE PD3 = ".$spec_score,true));
				$spec_score = $this->translateScoreNewCalculation($spec_score);
				$score_PS = intval($score_PS + $spec_score);
                //error_log(print_r("PS6 = ".$spec_score,true));
			}else if($code == 'PS4'){//PC7
				$spec_score = intval($answer_list[$i]);
                $score_PS = intval($score_PS - $spec_score);// remove PS4
                //error_log(print_r("REMOVE PS4 = ".$spec_score,true));
				$spec_score = $this->translateScoreNewCalculation($spec_score);
				$score_PC = intval($score_PC + $spec_score);
                //error_log(print_r("PC7 = ".$spec_score,true));
			}else if($code == 'YL12'){//PB9, YL12_1
				$spec_score = intval($answer_list[$i]);
                $scoreYL12= $spec_score;
                $score_YL = intval($score_YL - $spec_score);// remove YL12
                //error_log(print_r("REMOVE YL12 AND PUT TO satta = ".$spec_score,true));
				$spec_score = $this->translateScoreNewCalculation($spec_score);
                $scoreYL12_1 = $spec_score;

				$score_PB = intval($score_PB + $spec_score);// add PB9
                //error_log(print_r("PB9 = ".$spec_score,true));
                //error_log(print_r("YL12_1 TO moha = ".$spec_score,true));
			}
            //error_log(print_r("i = ".$i + 1,true));
		}
        //โทสะ 
		$tosa = $score_PA + $score_YA + $scoreYA13 + $scoreYA15 + $scoreYA16;
		//ราคะ
		$laka = $score_PD + $score_YD;
        
        //error_log(print_r("score_PD = ".$score_PD,true));
        //error_log(print_r("score_YD = ".$score_YD,true));
		//โมหะ
		$moha = $score_PL + $score_YL + $scoreYL12_1;
		//วิตก
		$vitok = $score_PB + $score_YD;
		//พุทธะ
		$putta = $score_PC + $score_YA;
		//ศรัทธา
        //error_log(print_r("score_PS = ".$score_PS,true));
        //error_log(print_r("score_YL = ".$score_YL,true));
        //error_log(print_r("scoreYL12 = ".$scoreYL12,true));
		$satta = $score_PS + $score_YL + $scoreYL12;

		//x
		$score_YD_PD = $laka / 16;
		$score_YA_PA = $tosa / 19;
		$score_YL_PL = $moha / 15;
		$score_YD_PB = $vitok / 21;
		$score_YL_PS = $satta / 17;
		$score_YA_PC = $putta / 19;
        // error_log(print_r("_laka = ".$laka,true));
        // error_log(print_r("_tosa = ".$tosa,true));
        // error_log(print_r("_moha = ".$moha,true));
        // error_log(print_r("_vitok = ".$vitok,true));
        // error_log(print_r("_satta = ".$satta,true));
        // error_log(print_r("_putta = ".$putta,true));
        // error_log(print_r("laka = ".$score_YD_PD,true));
        // error_log(print_r("tosa = ".$score_YA_PA,true));
        // error_log(print_r("moha = ".$score_YL_PL,true));
        // error_log(print_r("vitok = ".$score_YD_PB,true));
        // error_log(print_r("satta = ".$score_YL_PS,true));
        // error_log(print_r("putta = ".$score_YA_PC,true));
		//x square
		$YD_PD_square = $score_YD_PD * $score_YD_PD;
		$YA_PA_square = $score_YA_PA * $score_YA_PA;
		$YL_PL_square = $score_YL_PL * $score_YL_PL;
		$YD_PB_square = $score_YD_PB * $score_YD_PB;
		$YL_PS_square = $score_YL_PS * $score_YL_PS;
		$YA_PC_square = $score_YA_PC * $score_YA_PC;

		//sigma x then square
		$sigma_then_square_YD_PD = floatval($previous_calculation["sigma_YD_PD"]) + $score_YD_PD;
        //error_log(print_r("previous_calculation YD_PD = ".floatval($previous_calculation["sigma_YD_PD"]),true));
		$form_data["sigma_YD_PD"] = $sigma_then_square_YD_PD;
        //error_log(print_r("form_data YD_PD = ".$form_data["sigma_YD_PD"],true));
		$sigma_then_square_YD_PD = $sigma_then_square_YD_PD * $sigma_then_square_YD_PD;

		$sigma_then_square_YA_PA = floatval($previous_calculation["sigma_YA_PA"]) + $score_YA_PA;
		$form_data["sigma_YA_PA"] = $sigma_then_square_YA_PA;
		$sigma_then_square_YA_PA = $sigma_then_square_YA_PA * $sigma_then_square_YA_PA;

		$sigma_then_square_YL_PL = floatval($previous_calculation["sigma_YL_PL"]) + $score_YL_PL;
		$form_data["sigma_YL_PL"] = $sigma_then_square_YL_PL;
		$sigma_then_square_YL_PL = $sigma_then_square_YL_PL * $sigma_then_square_YL_PL;

		$sigma_then_square_YD_PB = floatval($previous_calculation["sigma_YD_PB"]) + $score_YD_PB;
		$form_data["sigma_YD_PB"] = $sigma_then_square_YD_PB;
		$sigma_then_square_YD_PB = $sigma_then_square_YD_PB * $sigma_then_square_YD_PB;

		$sigma_then_square_YL_PS = floatval($previous_calculation["sigma_YL_PS"]) + $score_YL_PS;
        //error_log(print_r("previous_calculation YL_PS = ".floatval($previous_calculation["sigma_YL_PS"]),true));
		$form_data["sigma_YL_PS"] = $sigma_then_square_YL_PS;
        //error_log(print_r("form_data YL_PS = ".$form_data["sigma_YL_PS"],true));
		$sigma_then_square_YL_PS = $sigma_then_square_YL_PS * $sigma_then_square_YL_PS;

		$sigma_then_square_YA_PC = floatval($previous_calculation["sigma_YA_PC"]) + $score_YA_PC;
		$form_data["sigma_YA_PC"] = $sigma_then_square_YA_PC;
		$sigma_then_square_YA_PC = $sigma_then_square_YA_PC * $sigma_then_square_YA_PC;

		//x square then sigma
		$square_then_sigma_YD_PD = floatval($previous_calculation["sigma_YD_PD_square"]) + $YD_PD_square;
		$square_then_sigma_YA_PA = floatval($previous_calculation["sigma_YA_PA_square"]) + $YA_PA_square;
		$square_then_sigma_YL_PL = floatval($previous_calculation["sigma_YL_PL_square"]) + $YL_PL_square;
		$square_then_sigma_YD_PB = floatval($previous_calculation["sigma_YD_PB_square"]) + $YD_PB_square;
		$square_then_sigma_YL_PS = floatval($previous_calculation["sigma_YL_PS_square"]) + $YL_PS_square;
		$square_then_sigma_YA_PC = floatval($previous_calculation["sigma_YA_PC_square"]) + $YA_PC_square;
		$form_data["sigma_YD_PD_square"] = $square_then_sigma_YD_PD;
		$form_data["sigma_YA_PA_square"] = $square_then_sigma_YA_PA;
		$form_data["sigma_YL_PL_square"] = $square_then_sigma_YL_PL;
		$form_data["sigma_YD_PB_square"] = $square_then_sigma_YD_PB;
		$form_data["sigma_YL_PS_square"] = $square_then_sigma_YL_PS;
		$form_data["sigma_YA_PC_square"] = $square_then_sigma_YA_PC;

		$total = intval($previous_calculation["total_participant"]);
		//เอาจำนวน ณ ตัวนี้ด้วยใส่ใน db temp
		$total = $total + 1;
		$form_data["total_participant"] = $total;
		$degree_freedom = $total - 1;
		if($total == 1){
			$degree_freedom = $total;
		}

		//calculate SD
		$sd_YD_PD = sqrt(($square_then_sigma_YD_PD + ($sigma_then_square_YD_PD / $total)) / ($degree_freedom));
		$sd_YA_PA = sqrt(($square_then_sigma_YA_PA + ($sigma_then_square_YA_PA / $total)) / ($degree_freedom));
		$sd_YL_PL = sqrt(($square_then_sigma_YL_PL + ($sigma_then_square_YL_PL / $total)) / ($degree_freedom));
		$sd_YD_PB = sqrt(($square_then_sigma_YD_PB + ($sigma_then_square_YD_PB / $total)) / ($degree_freedom));
		$sd_YL_PS = sqrt(($square_then_sigma_YL_PS + ($sigma_then_square_YL_PS / $total)) / ($degree_freedom));
		$sd_YA_PC = sqrt(($square_then_sigma_YA_PC + ($sigma_then_square_YA_PC / $total)) / ($degree_freedom));
        
        //error_log(print_r("\n",true));

		//calculate x bar
		$x_bar_YD_PD = $form_data["sigma_YD_PD"] / $total;
		$x_bar_YA_PA = $form_data["sigma_YA_PA"] / $total;
		$x_bar_YL_PL = $form_data["sigma_YL_PL"] / $total;
		$x_bar_YD_PB = $form_data["sigma_YD_PB"] / $total;
		$x_bar_YL_PS = $form_data["sigma_YL_PS"] / $total;
		$x_bar_YA_PC = $form_data["sigma_YA_PC"] / $total;

		//calculate z-score
		$z_score_YD_PD = $x_bar_YD_PD / $sd_YD_PD;
		$z_score_YA_PA = $x_bar_YA_PA / $sd_YA_PA;
		$z_score_YL_PL = $x_bar_YL_PL / $sd_YL_PL;
		$z_score_YD_PB = $x_bar_YD_PB / $sd_YD_PB;
		$z_score_YL_PS = $x_bar_YL_PS / $sd_YL_PS;
		$z_score_YA_PC = $x_bar_YA_PC / $sd_YA_PC;

		//calculate t-score
		$t_score_YD_PD = ($z_score_YD_PD * 10) + 50;
		$t_score_YA_PA = ($z_score_YA_PA * 10) + 50;
		$t_score_YL_PL = ($z_score_YL_PL * 10) + 50;
		$t_score_YD_PB = ($z_score_YD_PB * 10) + 50;
		$t_score_YL_PS = ($z_score_YL_PS * 10) + 50;
		$t_score_YA_PC = ($z_score_YA_PC * 10) + 50;

        // error_log(print_r("*** laka ***",true));
        // error_log(print_r("x = ".$score_YD_PD,true));
        // error_log(print_r("Sigma of exp(x) = ".$square_then_sigma_YD_PD,true));
        // error_log(print_r("exp(Sigma of x) = ".$sigma_then_square_YD_PD,true));
        // error_log(print_r("Sample standard deviation = ".$sd_YD_PD,true));
        // error_log(print_r("x bar = ".$x_bar_YD_PD,true));
        // error_log(print_r("z-score = ".$z_score_YD_PD,true));
        // error_log(print_r("t-score = ".$t_score_YD_PD,true));
        // error_log(print_r("\n",true));

        // error_log(print_r("*** tosa ***",true));
        // error_log(print_r("x = ".$score_YA_PA,true));
        // error_log(print_r("Sigma of exp(x) = ".$square_then_sigma_YA_PA,true));
        // error_log(print_r("exp(Sigma of x) = ".$sigma_then_square_YA_PA,true));
        // error_log(print_r("Sample standard deviation = ".$sd_YA_PA,true));
        // error_log(print_r("x bar = ".$x_bar_YA_PA,true));
        // error_log(print_r("z-score = ".$z_score_YA_PA,true));
        // error_log(print_r("t-score = ".$t_score_YA_PA,true));
        // error_log(print_r("\n",true));

        // error_log(print_r("*** moha ***",true));
        // error_log(print_r("x = ".$score_YL_PL,true));
        // error_log(print_r("Sigma of exp(x) = ".$square_then_sigma_YL_PL,true));
        // error_log(print_r("exp(Sigma of x) = ".$sigma_then_square_YL_PL,true));
        // error_log(print_r("Sample standard deviation = ".$sd_YL_PL,true));
        // error_log(print_r("x bar = ".$x_bar_YL_PL,true));
        // error_log(print_r("z-score = ".$z_score_YL_PL,true));
        // error_log(print_r("t-score = ".$t_score_YL_PL,true));
        // error_log(print_r("\n",true));

        // error_log(print_r("*** vitok ***",true));
        // error_log(print_r("x = ".$score_YD_PB,true));
        // error_log(print_r("Sigma of exp(x) = ".$square_then_sigma_YD_PB,true));
        // error_log(print_r("exp(Sigma of x) = ".$sigma_then_square_YD_PB,true));
        // error_log(print_r("Sample standard deviation = ".$sd_YD_PB,true));
        // error_log(print_r("x bar = ".$x_bar_YD_PB,true));
        // error_log(print_r("z-score = ".$z_score_YD_PB,true));
        // error_log(print_r("t-score = ".$t_score_YD_PB,true));
        // error_log(print_r("\n",true));

        // error_log(print_r("*** putta ***",true));
        // error_log(print_r("x = ".$score_YA_PC,true));
        // error_log(print_r("Sigma of exp(x) = ".$square_then_sigma_YA_PC,true));
        // error_log(print_r("exp(Sigma of x) = ".$sigma_then_square_YA_PC,true));
        // error_log(print_r("Sample standard deviation = ".$sd_YA_PC,true));
        // error_log(print_r("x bar = ".$x_bar_YA_PC,true));
        // error_log(print_r("z-score = ".$z_score_YA_PC,true));
        // error_log(print_r("t-score = ".$t_score_YA_PC,true));
        // error_log(print_r("\n",true));

        // error_log(print_r("*** satta ***",true));
        // error_log(print_r("x = ".$score_YL_PS,true));
        // error_log(print_r("Sigma of exp(x) = ".$square_then_sigma_YL_PS,true));
        // error_log(print_r("exp(Sigma of x) = ".$sigma_then_square_YL_PS,true));
        // error_log(print_r("Sample standard deviation = ".$sd_YL_PS,true));
        // error_log(print_r("x bar = ".$x_bar_YL_PS,true));
        // error_log(print_r("z-score = ".$z_score_YL_PS,true));
        // error_log(print_r("t-score = ".$t_score_YL_PS,true));
        // error_log(print_r("\n",true));

        // error_log(print_r("n = ".$total,true));

		$graph_data = array(
			"t_score_YD_PD" => $t_score_YD_PD,
			"t_score_YA_PA" => $t_score_YA_PA,
			"t_score_YL_PL" => $t_score_YL_PL,
			"t_score_YD_PB" => $t_score_YD_PB,
			"t_score_YL_PS" => $t_score_YL_PS,
			"t_score_YA_PC" => $t_score_YA_PC
        );

        $cal_result["result"] = 1;
        $cal_result["data"] = $form_data;
        return $cal_result;
	}

    public function translateScoreNewCalculation($spec_score){
		if($spec_score == 5){
			$spec_score = 1;
		}else if($spec_score == 1){
			$spec_score = 5;
		}else if($spec_score == 4){
			$spec_score = 2;
		}else if($spec_score == 2){
			$spec_score = 4;
		}
		return $spec_score;
	}

	public function deleteAllData(){
		$resp = array();
		$qry = $this->conn->query("DELETE FROM answer_sheet");
		if(isset($qry)){
            $qry = null;
            $qry = $this->conn->query("DELETE FROM temp_calculation");
            if(isset($qry)){
                $resp = array(
                    "result" => 1
                );
            }else {
                $resp = array(
                    "result" => 2
                );
            }
		}else {
			$resp = array(
				"result" => 2
			);
		}
		return json_encode($resp);
		exit;
	}
}

$Master = new Master();
$action = !isset($_GET['f']) ? 'none' : strtolower($_GET['f']);
$sysset = new SystemSettings();
switch ($action) {
	case 'get_questions':
		echo $Master->get_questions();
	break;
	case 'save_answer':
		echo $Master->save_answer();
	break;
	case 'update_temp':
		echo $Master->update_temp();
	break;
    case 'import_data':
		echo $Master->uploadExcel();
	break;
	case 'export_excel':
		echo $Master->exportDataToExcel();
	break;
	case 'get_all_data':
		echo $Master->getAllData();
	break;
	case 'delete_all_data':
		echo $Master->deleteAllData();
	break;
	default:
		// echo $sysset->index();
		break;
}