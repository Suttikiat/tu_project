-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tu_stat_questionaire_db`
--

create database tu_stat_questionaire_db;
use tu_stat_questionaire_db;

-- --------------------------------------------------------

--
-- Table structure for table `system_info`
--

CREATE TABLE `system_info` (
  `id` int(30) NOT NULL,
  `meta_field` text NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `system_info`
--

INSERT INTO `system_info` (`id`, `meta_field`, `meta_value`) VALUES
(1, 'name', 'Questionnaire for statistic data'),
(6, 'short_name', 'Question'),
(11, 'logo', 'uploads/1620181980_bot2.jpg');
-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question` text DEFAULT NULL,
  `type` ENUM('1','2') NOT NULL DEFAULT '1' ,
  `question_code` VARCHAR(100) NOT NULL ,
  `status` ENUM('Y','N') NOT NULL DEFAULT 'Y' , 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `type`, `question_code`, `status`) VALUES
(1, 'การแต่งกายของท่านมีผลต่อความสำเร็จ', '1', 'YD1', 'Y'),
(2, 'ท่านจะตัดบททันทีเมื่อคนพูดไม่ตรงประเด็น', '1', 'YA12', 'Y'),
(3, 'ท่านจะไม่ยอมเปลี่ยนความคิดง่ายๆ', '1', 'YA9', 'Y'),
(4, 'ท่านชอบกิจกรรมโลดโผน', '1', 'YA7', 'Y'),
(5, 'ท่านชอบคิด วิเคราะห์ แก้ปัญหา', '1', 'YA11', 'Y'),
(6, 'ท่านชอบเดินเล่นไปเรื่อยๆ โดยไม่มีความทุกข์ร้อน', '1', 'YL2', 'Y'),
(7, 'ท่านชอบทำงานตามกฎระเบียบ', '1', 'YD7', 'Y'),
(8, 'ท่านเชื่อผู้อื่นได้ง่าย', '1', 'YL12', 'Y'),
(9, 'ท่านดูแลเครื่องนอนให้เรียบร้อยทุกคร้ัง', '1', 'YD5', 'Y'),
(40, 'ท่านทำงานด้วยความปรานีต', '1', 'YD8', 'Y'),
(41, 'ท่านทำงานได้ดีโดยเฉพาะงานประจำ', '1', 'YL7', 'Y'),
(42, 'ท่านป็นคนขี้หงุดหงิดและโกรธง่าย', '1', 'YA5', 'Y'),
(43, 'ท่านเป็นคนเด็ดขาด', '1', 'YA8', 'Y'),
(44, 'ท่านเป็นคนพูดจริงทำจริง', '1', 'YA10', 'Y'),
(45, 'ท่านเป็นคนพูดจาโผงผาง', '1', 'YA1', 'Y'),
(46, 'ท่านเป็นคนพูดจาไพเราะอ่อนหวาน', '1', 'YD4', 'Y'),
(47, 'ท่านเป็นคนรับประทานอาหารเร็ว', '1', 'YA6', 'Y'),
(48, 'ท่านมักทำอะไรด้วยความรวดเร็ว', '1', 'YA2', 'Y'),
(49, 'ท่านมักทำอะไรตามกลุ่มเพื่อน', '1', 'YL10', 'Y'),
(50, 'ท่านมักเร่งรีบทำงานให้เสร็จ', '1', 'YA4', 'Y'),
(51, 'ท่านมีความสุขทุกครั้งเมื่อได้ชมสิ่งที่บันเทิงอารมณ์', '1', 'YD2', 'Y'),
(52, 'ท่านมีอาการง่วงๆซึมๆบ่อย', '1', 'YL4', 'Y'),
(53, 'ท่านไม่ค่อยมีสมาธิในการทำงาน', '1', 'YL9', 'Y'),
(54, 'ท่านไม่ชอบการเป็นผู้นำ', '1', 'YL8', 'Y'),
(55, 'ท่านไม่ชอบงานที่ต้องรับผิดชอบมาก', '1', 'YD6', 'Y'),
(56, 'ท่านไม่ชอบทำตัวเป็นจุดเด่น', '1', 'YL1', 'Y'),
(57, 'ท่านเร่งรีบในการทำกิจวัตรเสมอ', '1', 'YD9', 'Y'),
(58, 'ท่านสามารถชื่นชมสิ่งรอบตัวเสมอ', '1', 'YD10', 'Y'),
(59, 'ท่านสามารถใช้คำพูดโต้แย้งได้อย่างเหมาะสม', '1', 'YD11', 'Y'),
(60, 'ท่านสามารถประสานเรื่องต่างๆได้ดี', '1', 'YD12', 'Y'),
(61, 'ท่านเหม่อลอยบ่อย', '1', 'YL3', 'Y'),
(62, 'ท่านให้ความสำคัญของสีและกลิ่นของอาหาร', '1', 'YD3', 'Y'),
(63, 'ท่านอยากทำสิ่งใด ท่านจะลงมือทำทันที', '1', 'YA3', 'Y'),
(64, 'บ่อยครั้งที่ท่านคิดวนไปวนมา', '1', 'YL11', 'Y'),
(65, 'เวลาตื่นนอนท่านจะเซื่องซึมงัวเงีย', '1', 'YL6', 'Y'),
(66, 'เวลาท่านทานอาหาร ท่านจะมองไปรอบๆ', '1', 'YL5', 'Y'),
(67, 'ท่านกังวลกับสิ่งที่คนอื่นไม่กังวล', '2', 'PS5', 'Y'),
(68, 'ท่านใคร่ครวญทุกคร้ังก่อนลงมือทำก่อน', '2', 'PC4', 'Y'),
(69, 'ท่านชอบจินตนาการในเรื่องต่างๆเสมอ', '2', 'PS1', 'Y'),
(70, 'ท่านชอบตกแต่งสิ่งรอบตัวท่านให้สวยงาม', '2', 'PD2', 'Y'),
(71, 'ท่านชอบเปลี่ยนที่ทำงานไปตามความคิดท่าน', '2', 'PS2', 'Y'),
(72, 'ท่านชอบพูดเรื่องสัพเพเหระ', '2', 'PS3', 'Y'),
(73, 'ท่านเชื่อมั่นในการเสียสละ', '2', 'PB1', 'Y'),
(74, 'ท่านเชื่อมั่นในสิ่งที่จิตสัมผัสได้', '2', 'PB6', 'Y'),
(75, 'ท่านทนไม่ได้เมื่อถูกเอาเปรียบ', '2', 'PA1', 'Y'),
(76, 'ท่านทำงานตามความเป็นจริง', '2', 'PC5', 'Y'),
(77, 'ท่านทำทุกอย่างที่จะหลุดพ้นจากความทุกข์', '2', 'PB2', 'Y'),
(78, 'ท่านเป็นคนขี้รำคาญ', '2', 'PL2', 'Y'),
(79, 'ท่านเป็นคนใครดี ดีด้วย ใครร้าย ร้ายตอบ', '2', 'PA2', 'Y'),
(80, 'ท่านเป็นคนมีเป้าหมายชีวิต', '2', 'PC6', 'Y'),
(81, 'ท่านเป็นคนละเอียดถี่ถ้วน', '2', 'PS4', 'Y'),
(82, 'ท่านพิถีพิถันในความสวยความงาม', '2', 'PD1', 'Y'),
(83, 'ท่านมักเก็บอารมณ์โกรธไม่ได้', '2', 'PA4', 'Y'),
(84, 'ท่านมีความมุ่งมั่นและพร้อมสนับสนุนในเรื่องที่ดีงาม', '2', 'PB5', 'Y'),
(85, 'ท่านมีความสุขมากเมื่อได้ฟังเรื่องราวของผู้คนผ่านพ้นความทุกข์', '2', 'PB3', 'Y'),
(86, 'ท่านไม่สร้างความเดือดร้อนให้กับตนเอง', '2', 'PC2', 'Y'),
(87, 'ท่านไม่สามารถทนได้เมื่อถูกมองเป็นคนขี้เหร่', '2', 'PD4', 'Y'),
(88, 'ท่านยอมรับฟังข้อแนะนำสั่งสอนที่เป็นประโยชน์ต่อตัวท่านเองเสมอ', '2', 'PC1', 'Y'),
(89, 'ท่านยึดมั่นในความเชื่อของตน', '2', 'PL1', 'Y'),
(90, 'ท่านรู้สึกเป็นสุขใจในการทำความดี', '2', 'PD3', 'Y'),
(91, 'ท่านรู้สึกว่าตนเองมีความสามารถสูง', '2', 'PA3', 'Y'),
(92, 'ท่านสร้างสมคุณงามความดีเสมอ', '2', 'PC3', 'Y'),
(93, 'ท่านอึดอัดใจเมื่อต้องพูดถึงคุณงามความดีของตนเอง', '2', 'PB4', 'Y'),
(94, 'ทำดีได้ดี ทำชั่วได้ชั่ว ไม่เป็นเรื่องจริงเสมอไป', '2', 'PL3', 'Y');

--
-- Table structure for table `answer_sheet`
--

CREATE TABLE `answer_sheet` (
  `id` INT(10) NOT NULL AUTO_INCREMENT , 
  `answer_part2_1` VARCHAR(255) NOT NULL , 
  `answer_part2_2` VARCHAR(255) NOT NULL , 
  `create_datetime` DATETIME NOT NULL , 
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_unicode_ci;

CREATE TABLE `temp_calculation` (
  `id` INT(10) NOT NULL AUTO_INCREMENT , 
  `sigma_YD_PD` DOUBLE(15,4) NOT NULL , 
  `sigma_YD_PD_square` DOUBLE(15,4) NOT NULL , 
  `sigma_YA_PA` DOUBLE(15,4) NOT NULL , 
  `sigma_YA_PA_square` DOUBLE(15,4) NOT NULL , 
  `sigma_YL_PL` DOUBLE(15,4) NOT NULL , 
  `sigma_YL_PL_square` DOUBLE(15,4) NOT NULL , 
  `sigma_YD_PB` DOUBLE(15,4) NOT NULL , 
  `sigma_YD_PB_square` DOUBLE(15,4) NOT NULL , 
  `sigma_YL_PS` DOUBLE(15,4) NOT NULL , 
  `sigma_YL_PS_square` DOUBLE(15,4) NOT NULL , 
  `sigma_YA_PC` DOUBLE(15,4) NOT NULL , 
  `sigma_YA_PC_square` DOUBLE(15,4) NOT NULL , 
  `total_participant` INT(20) NOT NULL , 
  `update_datetime` DATETIME NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_unicode_ci;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
